(require 'cl-lib)

(defun simple-splash ()
  "Simple splash screen"

  (interactive)
  (let* ((splash-buffer  (get-buffer-create "*splash*"))
         (height         (- (window-body-height nil) 1))
         (width          (window-body-width nil))
         (padding-center (- (/ height 2) 1))
         (padding-bottom (- height (/ height 2) 3)))

    (if (eq 0 (length (cl-loop for buf in (buffer-list)
                               if (buffer-file-name buf)
                               collect (buffer-file-name buf))))

        (with-current-buffer splash-buffer
          (erase-buffer)

          (if (one-window-p)
              (setq mode-line-format nil))
          (setq cursor-type nil)
          (setq vertical-scroll-bar nil)
          (setq horizontal-scroll-bar nil)
          (setq fill-column width)
          (face-remap-add-relative 'link :underline nil)

          (insert-char ?\n padding-center)

          (insert (concat
                   "GNU Emacs version "
                   (format "%d.%d" emacs-major-version emacs-minor-version)))
          (center-line) (insert "\n")
          (insert (propertize "An extensible, customizable, free/libre text editor — and more" 'face 'shadow))
          (center-line)

          (insert-char ?\n padding-bottom)

          (insert-text-button " www.gnu.org "
                              'action (lambda (_) (browse-url "https://www.gnu.org/software/emacs/"))
                              'help-echo "visit emacs website"
                              'follow-link t)
          (center-line) (insert "\n")

          (goto-char 0)
          (read-only-mode t)

          (local-set-key (kbd "q")         'simple-splash-kill)
          (local-set-key (kbd "<mouse-1>") 'simple-splash-kill)
          (display-buffer-same-window splash-buffer nil)))))

(defun simple-splash-kill ()
  "Kill the splash screen buffer"
  (interactive)
  (if (get-buffer "*splash*")
      (kill-buffer "*splash*")))

;; suppress any startup message in the echo area
(run-with-idle-timer 0.05 nil (lambda() (message nil)))

(if (and (not (member "-no-splash"  command-line-args))
         (not (member "--file"      command-line-args))
         (not (member "--insert"    command-line-args))
         (not (member "--find-file" command-line-args))
         (not inhibit-startup-screen))
    (progn
      (add-hook 'window-setup-hook 'simple-splash)
      (setq inhibit-startup-screen t
            inhibit-startup-message t
            inhibit-startup-echo-area-message t)))

(provide 'simple-splash)

;;; gitconfig.el --- Git config mode

(define-derived-mode gitconf-mode conf-unix-mode "Conf[Git]"
  "Git config"
  )

(provide 'gitconfig)

;;; gitconfig.el ends here

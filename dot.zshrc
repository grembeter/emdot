# -*- mode: sh -*-

autoload -Uz compinit
compinit

HISTFILE=~/.zhist
HISTSIZE=10000
SAVEHIST=100000

setopt AUTO_PUSHD           # push the current directory visited on the stack
setopt PUSHD_IGNORE_DUPS    # do not store duplicates in the stack
setopt PUSHD_SILENT         # do not print the directory stack after pushd or popd
setopt HIST_SAVE_NO_DUPS    # when writing out the history file, older commands
                            # that duplicate newer ones are omitted
setopt HIST_IGNORE_SPACE    # remove command lines from the history list when
                            # the first character on the line is a space
setopt HIST_IGNORE_DUPS     # do not enter command lines into the history list
                            # if they are duplicates of the previous event
setopt histignorealldups

setopt notify
unsetopt beep
bindkey -e

typeset -U path

# prepend user path
path=("$HOME/bin" $path)
export PATH

if [ -e ~/.zkbd/$TERM-${${DISPLAY:t}:-$VENDOR-$OSTYPE} ]; then
    source ~/.zkbd/$TERM-${${DISPLAY:t}:-$VENDOR-$OSTYPE}
else
    echo '::::: autoload zkbd && zkbd'
fi

[[ -n "$key[Home]" ]]      && bindkey -- "$key[Home]"      beginning-of-line
[[ -n "$key[End]" ]]       && bindkey -- "$key[End]"       end-of-line
[[ -n "$key[Insert]" ]]    && bindkey -- "$key[Insert]"    overwrite-mode
[[ -n "$key[Delete]" ]]    && bindkey -- "$key[Delete]"    delete-char
[[ -n "$key[Backspace]" ]] && bindkey -- "$key[Backspace]" backward-delete-char
[[ -n "$key[Left]" ]]      && bindkey -- "$key[Left]"      backward-char
[[ -n "$key[Right]" ]]     && bindkey -- "$key[Right]"     forward-char

autoload -Uz up-line-or-beginning-search
zle -N up-line-or-beginning-search

autoload -Uz down-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "$key[Up]" ]]   && bindkey -- "$key[Up]"   up-line-or-beginning-search
[[ -n "$key[Down]" ]] && bindkey -- "$key[Down]" down-line-or-beginning-search

bindkey '^[[1;3D' emacs-backward-word             # M-Left
bindkey '^[[1;3C' emacs-forward-word              # M-Right
bindkey '^[[3;3~' kill-word                       # M-Del

z-chdir-parent() {
    cd -P ..
    zle reset-prompt
}

zle -N z-chdir-parent
bindkey '^[[1;5A' z-chdir-parent                  # C-Up

z-dumpttf() {
    ttfdump -t cmap -i "${1:-/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf}" \
        | perl -CS -ne 'print chr(hex($1)) if /Char (0x[[:xdigit:]]+)/ and hex($1) != 0xffff; END {print "\n"}'
}

z-set-prompt() {
    local prompt_color="${1:-$PROMPT_COLOR_DEFAULT}"
    prompt='%(?.%K{'"${prompt_color}"'}%F{black} ✔ %f%k %F{'"${prompt_color}"'}%m%f %F{'"${prompt_color}"'}Ξ%f.%K{red}%F{black} ✗ %f%k %F{'"${prompt_color}"'}%m%f %F{red}‼%f) %F{green}%/%f
 ; '
}

PROMPT_COLOR_DEFAULT="cyan"
PROMPT_COLOR_CROSS="011"

GPG_TTY="$TTY"
export GPG_TTY

alias 0='sudo'
alias 0v='sudo vim'

alias 1s='aptitude search'
alias 1h='aptitude show'
alias 1l='aptitude search "~U"'
alias 1p='aptitude install -y --without-recommends --simulate --show-versions'

alias 1u='sudo aptitude update && sudo aptitude search "~N"'
alias 1i='sudo aptitude install --prompt --without-recommends --show-versions'
alias 1g='sudo aptitude safe-upgrade'

alias cd='cd -P'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../..'                             # same as previous

alias ls='ls --color'
alias ll='ls --color -l'
alias l='ls --color -l --almost-all'

alias h='history 1 | grep --color -E -e'
alias df='df --human-readable --print-type --portability'
alias fx='find . -type f -print0 | xargs -n10 -0 grep --color -nH'
alias fn='find . -name'
alias fni='find . -iname'
alias cal='ncal -Mb'
alias grep='grep --color'
alias xclip='xclip -selection clipboard'
alias bat='batcat'

alias g='git'
alias gdif='git diff --no-index'
alias picog='picocom --imap lfcrlf --omap crlf'

alias cd0='dirs -v'
for index ({1..9}); do alias "cd$index"="cd +${index}"; done; unset index

alias reboot='sudo -k reboot'

z-set-prompt "$PROMPT_COLOR_DEFAULT"
